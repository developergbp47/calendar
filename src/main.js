import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import locale from 'element-plus/dist/locale/es.mjs'
import Dayjs from 'dayjs';

const app = createApp(App)

app.config.globalProperties.$dayjs = Dayjs;
app.use(ElementPlus,{
    locale
});

app.mount('#app')
